# Find-The-Number

_English_ 

It is a basic 'game' under console/terminal that must be run with ruby.
The goal? Find the exact number.

### Launch the game

- Download the `run_en.rb` 
- Open the Terminal
- Execute the script with Ruby.

_Français_

C'est un jeu basique sous console/terminal qui doit être exécuté avec Ruby.
Le but? Trouver le chiffre exact !

### Lancer le jeu

- Téléchargez `run.rb`
- Ouvrez votre terminal
- Exécutez le script avec Ruby.
